using System;
namespace Ejercicio11
{
    class Ejercicio11
    {
        private int[] sueldos;

        public void Cargar()
        {
            sueldos = new int[5];
            for (int f = 0; f < 5; f++)
            {
                Console.Write("Ingrese el sueldo:");
                String linea;
                linea = Console.ReadLine();
                sueldos[f] = int.Parse(linea);
            }
        }

        public void mostrar() 
        {
            for(int f = 0; f < 5; f++) 
            {
                Console.WriteLine(sueldos[f]);
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
          Ejercicio11 xd = new Ejercicio11();
           xd.Cargar();
            xd.mostrar();
        }
    }
}
